# Be.Windows.Forms.HexBox

#### 介绍
从https://sourceforge.net/projects/hexbox/下载的十六进制.net组件，对其进行了必要的功能增强和一些用户体验的Bug修复。


#### 增强以及修复整理

1.  增加撤销和恢复更改
2.  整理代码，分块处理
3.  增加一些属性和事件
4.  增强底色标注
5.  修复选择色块填充比较难受的填充
6.  增强底色填充功能
7.  增强分割线和修复其已知Bug
8.  增加缩放功能
9.  增加Unicode的支持

####  效果图

![效果图](https://images.gitee.com/uploads/images/2021/0725/224031_07c9dac4_7874578.png "效果图")

#### 声明

本代码遵循MIT开源协议，您可将其用于个人或商业用途，但必须附带原作者和增进开发者的姓名（wingsummer）
