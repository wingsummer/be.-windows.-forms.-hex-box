﻿using System;
using System.IO;

namespace Be.Windows.Forms
{
    public partial class HexBox
    {
        /// <summary>
        /// Opens a file.
        /// </summary>
        /// <param name="fileName">the file name of the file to open</param>
        /// <param name="writeable">是否可写权限</param>
        public bool OpenFile(string fileName, bool writeable = true)
        {
            if (!File.Exists(fileName))
                return false;
            CloseFile(false);
            try
            {
                DynamicFileByteProvider dynamicFileByteProvider;
                try
                {
                    dynamicFileByteProvider = new DynamicFileByteProvider(fileName, !writeable);
                }
                catch (IOException) // read-only also failed
                {
                    return false;
                }
                ByteProvider = dynamicFileByteProvider;
                Filename = fileName;
            }
            catch (Exception)
            {
                return false;
            }
            IsOpenedFile = true;
            return true;
        }

        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="filename">如果此参数为空，则为保存，否则为导出</param>
        /// <returns></returns>
        public bool SaveFile(string filename = null)
        {
            if (_byteProvider == null)
                return false;
            try
            {
                if (filename == null)
                {
                    _byteProvider.ApplyChanges(null);
                }
                else
                {
                    using (FileStream fileStream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        Stream stream = (_byteProvider as DynamicFileByteProvider).Stream;
                        stream.Position = 0;
                        stream.CopyTo(fileStream);
                        _byteProvider.ApplyChanges(fileStream);
                        fileStream.Flush();
                        fileStream.Close();
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 关闭文件
        /// </summary>
        /// <param name="IsSave">如果该参数为False，则丢弃更改</param>
        /// <returns></returns>
        public bool CloseFile(bool IsSave = true)
        {
            if (_byteProvider == null)
                return false;

            try

            {
                if (IsSave && _byteProvider != null && _byteProvider.HasChanges())
                {
                    SaveFile();
                }
                CleanUp();
            }
            catch
            {
                return false;
            }
            IsOpenedFile = false;
            return true;
        }

        private void CleanUp()
        {
            if (_byteProvider != null)
            {
                IDisposable byteProvider = _byteProvider as IDisposable;
                if (byteProvider != null)
                    byteProvider.Dispose();
                _byteProvider = null;
            }
            Filename = null;
        }
    }
}